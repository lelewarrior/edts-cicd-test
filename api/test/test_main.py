from fastapi.testclient import TestClient

from ..app.main import app

client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}

def test_read_test():
    response = client.get("/test")
    assert response.status_code == 200
    assert response.json() == {"msg": "This is test page"}

def test_read_new():
    response = client.get("/new")
    assert response.status_code == 200
    assert response.json() == {"msg": "This is new page"}