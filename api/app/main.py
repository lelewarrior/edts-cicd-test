from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def read_main():
    return {"msg": "Hello World"}

@app.get("/test")
async def read_test():
    return {"msg": "This is test page"}

@app.get("/new")
async def read_new():
    return {"msg": "This is new page"}